/** A C program to store the details of students using the knowledge of loops,
 *  arrays and structures you gained so far. First name of the student, 
 *  subject and its marks for each student needs to be recorded. Data 
 *  should be fetched through user input (do not hard code) and print the data 
 *  back. Program should record and display information of minimum 5 students.
 */

#include<stdio.h>

#define MAX_LENGTH 100 //constant to declare the maximum no students

/** Structure to hold data related to a student
 */
struct student
{
    char name[20];
    char subjectName[25];
    int marks;
};

/** function to display a structure
 */
void displayStruct(struct student *st)
{
    printf("\nName of the student\t: %s\n", st->name);
    printf("Subject\t\t\t: %s\n", st->subjectName);
    printf("Marks for the subject\t: %d\n", st->marks);
}

/** function to store the data in a structure
 */
void inputToStruct(struct student *st)
{
    printf("\nEnter the name for the student\t: ");
    scanf("%s", st->name);

    printf("Enter the subject\t\t: ");
    scanf("%s", st->subjectName);

    printf("Enter the marks\t\t\t: ");
    scanf("%d", &st->marks);
}


int main()
{
    int noOfStudents = 0;//variable to store the current student count from user
    struct student st[MAX_LENGTH]; //declaring a structure array

    /* prompting no of student from user*/
    printf("Enter the number of students : ");
    scanf("%d", &noOfStudents);

    /*checking whether user entered a valid no of students*/
    if((noOfStudents <= 0)){

        printf("\nEnter a valid number for students\n");

        return -1;
    }
    else
        {
            /* a loop to iterate through structure array
            * and store in a structure
            */
            for (int i = 0; i < noOfStudents; i++)
            {
                printf("\nEnter Details for student %d", i+1);
                inputToStruct(&st[i]);
            }

            /* a loop to iterate through structure array
            * and display details stored in a structure
            */
            for (int i = 0; i < noOfStudents; i++)
            {
                printf("\nDetails of student %d ", i + 1);
                displayStruct(&st[i]);
            }
            
        }

    return 0;
}
